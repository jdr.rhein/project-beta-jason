from django.urls import path
from .views import (api_list_customers,
                    api_customer_details,
                    api_list_salespeople,
                    api_salespeople_details,
                    api_list_sales,
                    api_sale_details,
                    )


urlpatterns = [
    path('customers/', api_list_customers, name='api_list_hats'),
    path('customers/<int:pk>/', api_customer_details, name='api_customer_details'),
    path('salespeople/', api_list_salespeople, name='api_list_salespeople'),
    path('salespeople/<int:pk>/', api_salespeople_details, name='api_salespeople_details'),
    path('sales/', api_list_sales, name='api_list_sales'),
    path('sales/<int:pk>/', api_sale_details, name='api_sale_details'),
]
