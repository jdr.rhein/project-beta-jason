# CarCar

Team:

* Liland Pham - Service
* Jason Rhein - Sales

## Design

## Service microservice

Created models for the Automobile (AutomobileVO), Technician, and Appointment. The technician is a foreign key to appointment. The polling occurs with the AutomobileVO which grabs the request from the Automobile model in the Inventory project.

For the front end of Service, I created both forms and lists for Technician and Appointment using React and Bootstrap. The Service Appointment lists has the 'Cancel', 'Finish', and 'Delete' functions while the Service History and Technician lists only incorporate the 'Delete' function. Both the Appointment lists will be fetching data from the inventory API in order to see if the VIN from the inventory matches the VIN coming from the appointment API. I also used styling in the JSX to enhance user experience.

For the front end of Inventory, I created both forms and lists for Models and Manufacturers using React and Bootstrap. I also used styling in the JSX to enhance user experience. Also added a footer to the bottom of each page. 

## Sales microservice

For the sales microservice, I created four models (AutomobileVO, Salesperson, Customer, Sales). The Sales model contains an Automobile foreign key, which is the go between for the sales and inventory microservice. The poller is responsible for allowing the sales microservice to use Automobile information, formatted as a value object.

List and Form pages were created for each model, as well as for the Automobile portion of the inventory microservice. The list pages will allow a user to view information already stored via the models in the database, while the form page allows a user to create new information for the corresponding element.

For the back end, I created Insomnia endpoints that correspond with view functions. With the view function / Insomnia combination, I'm able to list, create, and delete model data. 

For an extra element, Liland and I added a footer to each page. 
