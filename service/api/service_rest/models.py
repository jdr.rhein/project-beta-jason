from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=17, unique=True)

    def __str__(self):
        return self.vin


class Technician(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    employee_id = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.employee_id

    def get_api_url(self):
        return reverse("api_show_technician", kwargs={"pk": self.pk})


class Status(models.Model):
    name = models.CharField(max_length=10)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("id",)
        verbose_name_plural = "statuses"


class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=100)
    customer = models.CharField(max_length=150)
    vin = models.CharField(max_length=17, unique=True)
    status = models.ForeignKey(
        Status,
        related_name="status",
        on_delete=models.PROTECT
    )
    technician = models.ForeignKey(
        Technician,
        related_name="technician",
        on_delete=models.CASCADE
    )

    @classmethod
    def create(cls, **kwargs):
        kwargs["status"] = Status.objects.get(name="created")
        appointment = cls(**kwargs)
        appointment.save()
        return appointment

    def canceled(self):
        status = Status.objects.get(name="canceled")
        self.status = status
        self.save()

    def finished(self):
        status = Status.objects.get(name="finished")
        self.status = status
        self.save()

    def get_api_url(self):
        return reverse("api_show_appointment", kwargs={"pk": self.pk})

    def __str__(self):
        return self.vin
