import React, { useEffect, useState } from 'react';

function ModelsList() {
    const [models, setModels] = useState([]);

    const fetchData = async ()=> {
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setModels(data.models);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    const deleteModels = async (id) => {
        const url = `http://localhost:8100/api/models/${id}/`;
        const response = await fetch(url, { method: 'DELETE' });
        if (response.ok) {
            setModels(models.filter(model => model.id !== id));
        } else {
            console.error('Failed to delete a model');
        }
    };

    return (
        <div>
        <h2 style={{ marginTop: '20px' }}>Models</h2>
        <table className="table table-striped">
            <thead>
            <tr>
                <th>Name</th>
                <th>Manufacturer</th>
                <th>Picture</th>
            </tr>
            </thead>
            <tbody>
            {models.map((model) => {
            return (
                <tr key={model.href} value={model.id}>
                    <td>{model.name}</td>
                    <td>{model.manufacturer.name}</td>
                    <td>
                        <img src={model.picture_url} alt="http://placekitten.com/g/200/300" style={{width: "250px", height: "150px", objectFit: "cover"}}/>
                    </td>
                    <td>
                        <button type="button" className="btn btn-secondary btn-sm" style={{ backgroundColor: "#9DAAA2", border: "#9DAAA2" }} onClick={() => deleteModels(model.id)}>Delete</button>
                    </td>
                </tr>
            );
        })}
            </tbody>
        </table>
        </div>
    );
};

export default ModelsList;
