import React, { useEffect, useState } from 'react';

function ServiceAppointmentForm() {
    const [vin, setVin] = useState ('');
    const [customer, setCustomer] = useState ('');
    const [dateTime, setDateTime] = useState ('');
    const [reason, setReason] = useState ('');
    const [technician, setTechnician] = useState ('');
    const [technicians, setTechnicians] = useState ([]);

    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    };

    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    };

    const handleDateTimeChange = (event) => {
        const value = event.target.value;
        setDateTime(value);
    };

    const handleReasonChange = (event) => {
        const value = event.target.value;
        setReason(value);
    };

    const handleTechnicianChange = (event) => {
        const value = event.target.value;
        setTechnician(value);
    };

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/technicians/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.vin = vin;
        data.customer = customer;
        data.date_time = dateTime;
        data.reason = reason;
        data.technician = technician;

        const appointmentUrl = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch (appointmentUrl, fetchConfig);
        if (response.ok) {
            await response.json();
            setVin('');
            setCustomer('');
            setDateTime('');
            setReason('');
            setTechnician('');
        };
    };

    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h2>Create a Service Appointment</h2>
              <form onSubmit={handleSubmit} id="create-service-appointment-form">
              <label>Automobile VIN</label>
                <div className="form-floating mb-3">
                  <input onChange={handleVinChange} value={vin} placeholder="vin" required type="text" name="vin" id="vin" className="form-control"/>
                </div>
                <label>Customer</label>
                <div className="form-floating mb-3">
                  <input onChange={handleCustomerChange} value={customer} placeholder="customer" required type="text" name="customer" id="customer" className="form-control"/>
                </div>
                <label>Date and Time</label>
                <div className="form-floating mb-3">
                    <input onChange={handleDateTimeChange} value={dateTime} placeholder="Date" required type="datetime-local" name="date_time" id="date_time" className="form-control" style={{ opacity: 0.5 }}/>
                </div>
                <label>Technician</label>
                <div className="mb-3">
                  <select onChange={handleTechnicianChange} value={technician} className="form-select" required name="technician" id="technician">
                    <option value="" style={{ opacity: 0.5 }}>Choose a technician</option>
                    {technicians.map(technician => {
                        return (
                            <option key={technician.id} value={technician.id}>
                                {technician.first_name} {technician.last_name}
                            </option>
                        );
                    })}
                  </select>
                </div>
                <label>Reason</label>
                <div className="form-floating mb-3">
                  <input onChange={handleReasonChange} value={reason} placeholder="reason" required type="text" name="reason" id="reason" className="form-control"/>
                </div>
                <button className="btn btn-secondary">Create</button>
              </form>
            </div>
          </div>
        </div>
    );
};

export default ServiceAppointmentForm;
