import React, { useEffect, useState } from 'react';

function ServiceHistory() {
    const [appointments, setAppointments] = useState([]);
    const [autos, setAutos] = useState([]);
    const [filtered, setFiltered] = useState([]);
    const [search, setSearch] = useState("");

    const fetchAppointmentData = async ()=> {
        const url = 'http://localhost:8080/api/appointments/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments);
        }
    };

    const fetchVinData = async ()=> {
        const url = 'http://localhost:8100/api/automobiles/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setAutos(data.autos);
        }
    };

    useEffect(() => {
        fetchAppointmentData();
        fetchVinData();
    }, []);

    const handleSearchBar = (event) => {
        setSearch(event.target.value);
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        setFiltered(appointments.filter(appointment => appointment.vin.includes(search)));
    };

    const deleteServiceHistory = async (id) => {
        const url = `http://localhost:8080/api/appointments/${id}/`;
        const response = await fetch(url, { method: 'DELETE' });
        if (response.ok) {
            setAppointments(appointments.filter(appointment => appointment.id !== id));
        } else {
            console.error('Failed to delete service');
        };
    };


    return (
        <div className="row">
            <div className="col-12">
            <h2 style={{ marginTop: '20px' }}>Service History</h2>
            <form onSubmit={handleSubmit} id="search-form">
            <div className="form-floating mb-3" style={{ display: 'flex', alignItems: 'center' }}>
                <input onChange={handleSearchBar} value={search} placeholder="vin" type="text" name="vin" id="vin" className="form-control" style={{ marginRight: '1rem' }}/>
                <label htmlFor="vin" style={{ opacity: 0.5 }}>Search for VIN...</label>
                <button className="search btn btn-outline-secondary">Search</button>
            </div>
            </form>
        <div>
        <table className="table table-striped">
            <thead>
            <tr>
                <th>VIN</th>
                <th>Is VIP?</th>
                <th>Customer</th>
                <th>Date</th>
                <th>Time</th>
                <th>Technician</th>
                <th>Reason</th>
                <th>Status</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            {(filtered.length > 0 ? filtered : appointments).map((appointment) => {
                const vipVins = autos.some(auto => auto.vin === appointment.vin);
                const date = new Date(appointment.date_time).toLocaleDateString();
                const time = new Date(appointment.date_time).toLocaleTimeString();
            return (
                <tr key={appointment.href} value={appointment.id}>
                    <td>{appointment.vin}</td>
                    <td>{vipVins ? "Yes":"No"}</td>
                    <td>{appointment.customer}</td>
                    <td>{date}</td>
                    <td>{time}</td>
                    <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                    <td>{appointment.reason}</td>
                    <td>{appointment.status}</td>
                    <td>
                        <button type="button" className="btn btn-secondary btn-sm" style={{ backgroundColor: "#9DAAA2", border: "#9DAAA2" }}  onClick={() => deleteServiceHistory(appointment.id)}>Delete</button>
                    </td>
                </tr>
            );
        })}
            </tbody>
        </table>
        </div>
            </div>
        </div>
    );
};

export default ServiceHistory;
