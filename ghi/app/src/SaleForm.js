import React, {useEffect, useState} from 'react';

function SaleForm() {
    const [price, setPrice] = useState('');
    const [salesperson, setSalesperson] = useState('');
    const [customer, setCustomer] = useState('');
    const [automobile, setAutomobile] = useState('');
    const [salespeople, setSalespersons] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [automobiles, setAutomobiles] = useState([]);

    const handlePriceChange = (event) => {
        const value = event.target.value;
        setPrice(value);
    };

    const handleSalespersonChange = (event) => {
        const value = event.target.value;
        setSalesperson(value);
    };

    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    };

    const handleAutomobileChange = (event) => {
        const value = event.target.value;
        setAutomobile(value);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.price = price;
        data.salesperson = salesperson;
        data.customer = customer;
        data.automobile = automobile;

        const salesUrl = "http://localhost:8090/api/sales/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(salesUrl, fetchConfig);
        if (response.ok) {
            const newSale = await response.json();
            setPrice('');
            setSalesperson('');
            setCustomer('');
            setAutomobile('');
        };
    };

    const fetchData = async () => {
        const salespersonUrl = "http://localhost:8090/api/salespeople/";
        const customerUrl = "http://localhost:8090/api/customers/";
        const automobileUrl = "http://localhost:8100/api/automobiles/";
        const salespersonResponse = await fetch(salespersonUrl);
        const customerResponse = await fetch(customerUrl);
        const automobileResponse = await fetch(automobileUrl);

        if (salespersonResponse.ok && customerResponse.ok && automobileResponse.ok) {
            const salespersonData = await salespersonResponse.json();
            const customerData = await customerResponse.json();
            const automobileData = await automobileResponse.json();
            setSalespersons(salespersonData.salesperson);
            setCustomers(customerData.customer);
            setAutomobiles(automobileData.autos);
        };
    };

    useEffect(() => {
        fetchData();
    },[]);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Record a new Sale</h1>
                <form onSubmit={handleSubmit} id="create-sale-form">
                    <div className="form-floating mb-3">
                        <input onChange={handlePriceChange} value={price} placeholder="0" required type="number" name="price" id="price" className="form-control" />
                        <label htmlFor="price">Price</label>
                    </div>
                    <div className="form-floating mb-3">
                        <select onChange={handleSalespersonChange} value={salesperson} name="salesperson" id="salesperson" className="form-control">
                        <option value="">Choose a salesperson</option>
                            {salespeople.map(salesperson => {
                                return (
                                    <option key={salesperson.id} value={salesperson.id}>
                                        {salesperson.first_name} {salesperson.last_name}
                                    </option>
                                );
                            })}
                        </select>
                    </div>
                    <div className="form-floating mb-3">
                        <select onChange={handleCustomerChange} value={customer} name="customer" id="customer" className="form-control">
                        <option value="">Choose a customer</option>
                            {customers.map(customer => {
                                return (
                                    <option key={customer.id} value={customer.id}>
                                        {customer.first_name + " " + customer.last_name}
                                    </option>
                                );
                            })}
                        </select>
                    </div>
                    <div className="form-floating mb-3">
                        <select onChange={handleAutomobileChange} value={automobile} name="vin" id="vin" className="form-control">
                        <option value=" ">Choose an automobile</option>
                            {automobiles.map(automobile => {
                                return (
                                    <option key={automobile.vin} value={automobile.vin}>
                                        {automobile.vin}
                                    </option>
                                );
                            })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    </div>
    );
};

export default SaleForm;
