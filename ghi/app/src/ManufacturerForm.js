import React, { useState } from 'react';

function ManufacturerForm() {
    const [manufacturerName, setManufacturerName] = useState ('');

    const handleManufacturerNameChange = (event) => {
        const value = event.target.value;
        setManufacturerName(value);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = manufacturerName;
        const manufacturerUrl = 'http://localhost:8100/api/manufacturers/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch (manufacturerUrl, fetchConfig);
        if (response.ok) {
          await response.json();
            setManufacturerName('');
        }
    };

    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h2>Create a Manufacturer</h2>
              <form onSubmit={handleSubmit} id="create-manufacturer-form">
                <div className="form-floating mb-3">
                  <input onChange={handleManufacturerNameChange} value={manufacturerName} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                  <label htmlFor="name" style={{ opacity: 0.5 }}>Manufacturer Name...</label>
                </div>
                <button className="btn btn-secondary">Create</button>
              </form>
            </div>
          </div>
        </div>
    );
};

export default ManufacturerForm;
