import React, { useEffect, useState } from 'react';

function ModelForm() {
  const [modelName, setModelName] = useState ('');
  const [pictureUrl, setPictureUrl] = useState ('');
  const [manufacturer, setManufacturer] = useState ('');
  const [manufacturers, setManufacturers] = useState ([]);

  const handleModelNameChange = (event) => {
      const value = event.target.value;
      setModelName(value);
  };

  const handlePictureChange = (event) => {
      const value = event.target.value;
      setPictureUrl(value);
  };

  const handleManufacturerChange = (event) => {
      const value = event.target.value;
      setManufacturer(value);
  };

  const handleSubmit = async (event) => {
      event.preventDefault();
      const data = {};
      data.name = modelName;
      data.picture_url = pictureUrl;
      data.manufacturer_id = manufacturer;

      const modelUrl = 'http://localhost:8100/api/models/';
      const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
      };

      const response = await fetch(modelUrl, fetchConfig);
      if (response.ok) {
        await response.json();
          setModelName('');
          setPictureUrl('');
          setManufacturer('');
      }
  };

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

  return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h2>Create a Vehicle Model</h2>
            <form onSubmit={handleSubmit} id="create-model-form">
              <div className="form-floating mb-3">
                <input onChange={handleModelNameChange} value={modelName} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name" style={{ opacity: 0.5 }}>Model Name...</label>
              </div>
              <div className="form-floating mb-3">
                  <input onChange={handlePictureChange} value={pictureUrl} placeholder="Url" type="url" required name="picture_url" id="picture_url" className="form-control"/>
                  <label htmlFor="picture_url" style={{ opacity: 0.5 }}>Picture URL...</label>
              </div>
              <div className="mb-3">
                  <select onChange={handleManufacturerChange} value={manufacturer} className="form-select" required name="manufacturer" id="manufacturer">
                    <option value="">Choose a Manufacturer</option>
                    {manufacturers.map(manufacturer => {
                        return (
                            <option key={manufacturer.id} value={manufacturer.id}>
                                {manufacturer.name}
                            </option>
                        );
                    })}
                  </select>
              </div>
              <button className="btn btn-secondary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  };

export default ModelForm;
