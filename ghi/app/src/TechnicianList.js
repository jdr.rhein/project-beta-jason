import React, { useEffect, useState } from 'react';

function TechnicianList() {
    const [technicians, setTechnicians] = useState([]);

    const fetchData = async ()=> {
        const url = 'http://localhost:8080/api/technicians/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    const deleteTechnician = async (id) => {
        const url = `http://localhost:8080/api/technicians/${id}/`;
        const response = await fetch(url, { method: 'DELETE' });
        if (response.ok) {
            setTechnicians(technicians.filter(technician => technician.id !== id));
        } else {
            console.error('Failed to delete technician');
        }
    };

    return (
        <div>
            <h2 style={{ marginTop: '20px' }}>Technicians</h2>
            <table className="table table-striped">
                <thead>
                <tr>
                    <th>Employee ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                </tr>
                </thead>
                <tbody>
                {technicians.map((technician) => {
                return (
                    <tr key={technician.id} value={technician.id}>
                        <td>{technician.employee_id}</td>
                        <td>{technician.first_name}</td>
                        <td>{technician.last_name}</td>
                        <td>
                            <button type="button" className="btn btn-secondary btn-sm" style={{ backgroundColor: "#9DAAA2", border: "#9DAAA2" }} onClick={() => deleteTechnician(technician.id)}>Delete</button>
                        </td>
                    </tr>
                );
            })}
                </tbody>
            </table>
        </div>
    );
};

export default TechnicianList;
