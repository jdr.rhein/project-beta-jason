import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SalespersonList from './SalespersonList';
import SalespersonForm from './SalespersonForm';
import CustomerList from './CustomerList';
import SaleForm from './SaleForm';
import SalesList from './SalesList';
import SalespersonHistory from './SalespersonHistory';
import AutomobileForm from './AutomobileForm';
import AutomobileList from './AutomobileList';
import './Footer.css';
import TechnicianForm from './TechnicianForm';
import TechnicianList from './TechnicianList';
import ServiceAppointmentForm from './ServiceAppointmentForm';
import ServiceAppointmentList from './ServiceAppointment';
import ServiceHistory from './ServiceHistory';
import ManufacturerList from './ManufacturersList';
import ManufacturerForm from './ManufacturerForm';
import ModelsList from './ModelsList';
import ModelForm from './ModelForm';
import CustomerForm from './CustomerForm';
import './Style.css';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
            <Route path="salespeople/list" element={<SalespersonList />} />
            <Route path="customers/list" element={<CustomerList />} />
            <Route path="sales/list" element={<SalesList />} />
            <Route path="automobiles/list" element={<AutomobileList />} />
            <Route path="technicians/list" element={<TechnicianList />} />
            <Route path="appointments/list" element={<ServiceAppointmentList />} />
            <Route path="manufacturers/list" element={<ManufacturerList />} />
            <Route path="models/list" element={<ModelsList />} />
            <Route path="salespeople/new" element={<SalespersonForm />} />
            <Route path="salespeople/history" element={<SalespersonHistory />} />
            <Route path="customers/new" element={<CustomerForm />} />
            <Route path="sales/new" element={<SaleForm />} />
            <Route path="automobiles/new" element={<AutomobileForm />} />
            <Route path="technicians/create" element={<TechnicianForm />} />
            <Route path="appointments/create" element={<ServiceAppointmentForm />} />
            <Route path="appointments/history" element={<ServiceHistory />} />
            <Route path="manufacturers/create" element={<ManufacturerForm />} />
            <Route path="models/create" element={<ModelForm />} />
        </Routes>
        <footer className="footer">
          <div className="content-container">
            <p className="footer-text">Brought to you by Liland & Jason</p>
          </div>
        </footer>
      </div>
    </BrowserRouter>
  );
}

export default App;
