import React, {useEffect, useState} from 'react';

function SalespersonList() {
    const [salesperson, setSalesPerson] = useState([])

    const fetchData = async () => {
        const url = "http://localhost:8090/api/salespeople/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSalesPerson(data.salesperson)
        };
    };

    useEffect(() => {
        fetchData();
    },[]);

    return (
        <div>
            <h2 style={{ marginTop: "20px" }}>Salespeople</h2>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Employee ID</th>
                    </tr>
                </thead>
                <tbody>
                    {salesperson.map((salespers) => {
                        return (
                            <tr key={salespers.id} value={salespers.id}>
                                <td>{salespers.first_name}</td>
                                <td>{salespers.last_name}</td>
                                <td>{salespers.employee_id}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
};

export default SalespersonList;
