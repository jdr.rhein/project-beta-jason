import { NavLink } from 'react-router-dom';


function Nav() {

  return (
      <nav className="navbar navbar-expand-lg navbar-dark navcustom">
        <div className="container-fluid">
          <NavLink className="navbar-brand" to="/">CarCar</NavLink>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <div className="btn-group">
            <button type="button" className="btn btn-outline-light dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false" style={{ marginRight: "75px" }} >
              Automobile Inventory
            </button>
              <ul className="dropdown-menu">
                <li className="nav-item">
                  <NavLink className="dropdown-item" to="manufacturers/create">Add a Manufacturer</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="dropdown-item" to="models/create">Add a Model</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="dropdown-item" to="automobiles/new">Add an Automobile</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="dropdown-item" to="manufacturers/list">Manufacturers</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="dropdown-item" to="models/list">Models</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="dropdown-item" to="automobiles/list">Automobiles</NavLink>
                </li>
              </ul>
          </div>
          <div className="btn-group">
            <button type="button" className="btn btn-outline-light dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false" style={{ marginRight: "75px" }}>
              Salespeople
            </button>
            <ul className="dropdown-menu">
              <li className="nav-item">
                <NavLink className="dropdown-item" to="salespeople/new">Add a Salesperson</NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="dropdown-item" to="salespeople/list">Salespeople</NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="dropdown-item" to="salespeople/history">Salesperson History</NavLink>
              </li>
            </ul>
          </div>
          <div className="btn-group">
            <button type="button" className="btn btn-outline-light dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false" style={{ marginRight: "75px" }}>
              Sales
            </button>
            <ul className="dropdown-menu">
              <li className="nav-item">
                <NavLink className="dropdown-item" to="sales/new">Add a Sale</NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="dropdown-item" to="sales/list">Sales</NavLink>
              </li>
            </ul>
          </div>
          <div className="btn-group">
            <button type="button" className="btn btn-outline-light dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false" style={{ marginRight: "75px" }}>
              Customers
            </button>
            <ul className="dropdown-menu">
              <li className="nav-item">
                <NavLink className="dropdown-item" to="customers/new">Add a Customer</NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="dropdown-item" to="customers/list">Customers</NavLink>
              </li>
            </ul>
          </div>
          <div className="btn-group">
            <button type="button" className="btn btn-outline-light dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false" style={{ marginRight: "75px" }}>
              Technicians
            </button>
            <ul className="dropdown-menu">
              <li className="nav-item">
                  <NavLink className="dropdown-item" to="technicians/create">Add a Technician</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="dropdown-item" to="technicians/list">Technicians</NavLink>
              </li>
            </ul>
          </div>
          <div className="btn-group">
            <button type="button" className="btn btn-outline-light dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false" style={{ marginRight: "75px" }}>
              Service Appointments
            </button>
            <ul className="dropdown-menu">
              <li className="nav-item">
                <NavLink className="dropdown-item" to="appointments/create">Add a Service Appointment</NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="dropdown-item" to="appointments/list">Service Appointments</NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="dropdown-item" to="appointments/history">Service History</NavLink>
              </li>
            </ul>
          </div>
        </div> 
      </div>
    </nav>
  )
}

export default Nav;
