import React, { useEffect, useState } from 'react';

function ManufacturerList() {
    const [manufacturers, setManufacturers] = useState([]);

    const fetchData = async ()=> {
        const url = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    const deleteManufacturer = async (id) => {
        const url = `http://localhost:8100/api/manufacturers/${id}/`;
        const response = await fetch(url, { method: 'DELETE' });
        if (response.ok) {
            setManufacturers(manufacturers.filter(manufacturer => manufacturer.id !== id));
        } else {
            console.error('Failed to delete manufacturer');
        }
    };

    return (
        <div>
            <h2 style={{ marginTop: '20px' }}>Manufacturers</h2>
            <table className="table table-striped">
                <thead>
                <tr>
                    <th>Name</th>
                </tr>
                </thead>
                <tbody>
                {manufacturers.map((manufacturer) => {
                    return (
                        <tr key={manufacturer.id} value={manufacturer.id}>
                            <td>{manufacturer.name}</td>
                            <td>
                                <button type="button" className="btn btn-secondary btn-sm" style={{ backgroundColor: "#9DAAA2", border: "#9DAAA2" }} onClick={() => deleteManufacturer(manufacturer.id)}>Delete</button>
                            </td>
                        </tr>
                    );
                })}
                </tbody>
            </table>
        </div>
    );
};

export default ManufacturerList;
