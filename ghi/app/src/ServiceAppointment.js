import React, { useEffect, useState } from 'react';

function ServiceAppointmentList() {
    const [appointments, setAppointments] = useState([]);
    const [autos, setAutos] = useState([]);

    const fetchAppointmentData = async ()=> {
        const url = 'http://localhost:8080/api/appointments/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            const pending = data.appointments.filter(appointment => appointment.status === "created");
            setAppointments(pending);
        }
    };

    const fetchVinData = async ()=> {
        const url = 'http://localhost:8100/api/automobiles/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setAutos(data.autos);
        }
    };

    useEffect(() => {
        fetchAppointmentData();
        fetchVinData();
    }, []);

    const canceled = async (id) => {
        const url = `http://localhost:8080/api/appointments/${id}/cancel/`;
        const response = await fetch(url, { method: 'PUT' });
        if (response.ok) {
            setAppointments(appointments.filter(appointment => appointment.id !== id));
        } else {
            console.error('Failed to cancel appointment');
        }
    };

    const finished = async (id) => {
        const url = `http://localhost:8080/api/appointments/${id}/finish/`;
        const response = await fetch(url, { method: 'PUT' });
        if (response.ok) {
            setAppointments(appointments.filter(appointment => appointment.id !== id));
        } else {
            console.error('Failed to set appointment to finish');
        }
    };

    const deleteAppointment = async (id) => {
        const url = `http://localhost:8080/api/appointments/${id}/`;
        const response = await fetch(url, { method: 'DELETE' });
        if (response.ok) {
            setAppointments(appointments.filter(appointment => appointment.id !== id));
        } else {
            console.error('Failed to delete appointment');
        }
    };

    return (
        <div>
        <h2 style={{ marginTop: '20px' }}>Service Appointments</h2>
        <table className="table table-striped">
            <thead>
            <tr>
                <th>VIN</th>
                <th>Is VIP?</th>
                <th>Customer</th>
                <th>Date</th>
                <th>Time</th>
                <th>Technician</th>
                <th>Reason</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            {appointments.map((appointment) => {
                const vipVins = autos.some(auto => auto.vin === appointment.vin);
                const date = new Date(appointment.date_time).toLocaleDateString();
                const time = new Date(appointment.date_time).toLocaleTimeString();
            return (
                <tr key={appointment.href} value={appointment.id}>
                    <td>{appointment.vin}</td>
                    <td>{vipVins ? "Yes":"No"}</td>
                    <td>{appointment.customer}</td>
                    <td>{date}</td>
                    <td>{time}</td>
                    <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                    <td>{appointment.reason}</td>
                    <td>
                        <button type="button" className="btn btn-outline-danger" style={{ marginRight: '10px' }} onClick={() => canceled(appointment.id)}>Cancel</button>
                        <button type="button" className="btn btn-outline-success" style={{ marginRight: '10px' }} onClick={() => finished(appointment.id)}>Finish</button>
                        <button type="button" className="btn btn-outline-primary" onClick={() => deleteAppointment(appointment.id)}>Delete</button>
                    </td>
                </tr>
            );
        })}
            </tbody>
        </table>
        </div>
    );
};

export default ServiceAppointmentList;
